FROM continuumio/miniconda3:latest

MAINTAINER Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>


RUN apt-get -qq update && apt-get -qq upgrade && apt-get install -qq software-properties-common build-essential git && conda update --yes conda
RUN useradd -m py
WORKDIR /src


COPY environment.yml /src/
RUN conda create -q --name netcdf-scm


COPY . /src/


# Ensure that the correct conda environment is used
RUN sed -i 's/conda activate base/conda activate netcdf-scm/g' ~/.bashrc


# Create new conda environment and install our requirements too
RUN bash -c '. /opt/conda/etc/profile.d/conda.sh && conda activate netcdf-scm && make -B conda-environment && conda list && pip list' && conda clean -a -y

---- HEADER ----

Date: 2019-11-12 08:39:07
Contact: cmip6output wrangling regression test
Source data crunched with: NetCDF-SCM v0+untagged.12.g4182656 (more info at gitlab.com/znicholls/netcdf-scm)
File written with: pymagicc v2.0.0-beta.8 (more info at github.com/openclimatedata/pymagicc)


---- METADATA ----

Conventions: CF-1.5
area_world (m**2): 509499402446956.25
area_world_el_nino_n3.4 (m**2): 5603961331640.359
area_world_land (m**2): 146332759801316.12
area_world_north_atlantic_ocean (m**2): 39752089412013.26
area_world_northern_hemisphere (m**2): 254749701223478.12
area_world_northern_hemisphere_land (m**2): 98540203370717.92
area_world_northern_hemisphere_ocean (m**2): 156209497829702.06
area_world_ocean (m**2): 363166642636326.06
area_world_southern_hemisphere (m**2): 254749701223478.12
area_world_southern_hemisphere_land (m**2): 47792556430598.2
area_world_southern_hemisphere_ocean (m**2): 206957144806624.03
branch_method: branch
branch_time_in_child: 0.0
branch_time_in_parent: 0.0
calendar: 365_day
cmor_version: 3.3.2
comment: at the top of the atmosphere (to be compared with satellite measurements)
contact: Dr. Tongwen Wu (twwu@cma.gov.cn)
creation_date: 2018-10-15T06:27:37Z
crunch_contact: cmip6output crunching regression test
crunch_netcdf_scm_version: 0+untagged.12.g4182656 (more info at gitlab.com/znicholls/netcdf-scm)
crunch_source_files: Files: ['/CMIP6/CMIP/BCC/BCC-CSM2-MR/1pctCO2/r1i1p1f1/Amon/rlut/gn/v20181015/rlut_Amon_BCC-CSM2-MR_1pctCO2_r1i1p1f1_gn_185001-185912.nc']
data_specs_version: 01.00.27
description: DECK: 1pctCO2
experiment: 1 percent per year increase in CO2
experiment_id: 1pctCO2
external_variables: areacella
forcing_index: 1
frequency: mon
further_info_url: https://furtherinfo.es-doc.org/CMIP6.BCC.BCC-CSM2-MR.1pctCO2.none.r1i1p1f1
grid: T106
grid_label: gn
history: 2018-10-15T06:27:35Z ; CMOR rewrote data to be consistent with CMIP6, CF-1.7 CMIP-6.2 and CF standards.
initialization_index: 1
institution: Beijing Climate Center, Beijing 100081, China
institution_id: BCC
land_fraction: 0.28720889386430787
land_fraction_northern_hemisphere: 0.3868118506026193
land_fraction_southern_hemisphere: 0.18760593712599638
license: CMIP6 model data produced by BCC is licensed under a Creative Commons Attribution ShareAlike 4.0 International License (https://creativecommons.org/licenses). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file) and at https:///pcmdi.llnl.gov/. The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.
netcdf-scm crunched file: CMIP6/CMIP/BCC/BCC-CSM2-MR/1pctCO2/r1i1p1f1/Amon/rlut/gn/v20181015/netcdf-scm_rlut_Amon_BCC-CSM2-MR_1pctCO2_r1i1p1f1_gn_185001-185912.nc
nominal_resolution: 100 km
original_name: FLUT
parent_activity_id: CMIP
parent_experiment_id: piControl
parent_mip_era: CMIP6
parent_source_id: BCC-CSM2-MR
parent_time_units: days since 1850-01-01
parent_variant_label: r1i1p1f1
physics_index: 1
product: model-output
realization_index: 1
realm: atmos
references: Model described by Tongwen Wu et al. (JGR 2013; JMR 2014; submmitted to GMD,2018). Also see http://forecast.bcccsm.ncc-cma.net/htm
run_variant: forcing: GHG
source: BCC-CSM 2 MR (2017):   aerosol: none  atmos: BCC_AGCM3_MR (T106; 320 x 160 longitude/latitude; 46 levels; top level 1.46 hPa)  atmosChem: none  land: BCC_AVIM2  landIce: none  ocean: MOM4 (1/3 deg 10S-10N, 1/3-1 deg 10-30 N/S, and 1 deg in high latitudes; 360 x 232 longitude/latitude; 40 levels; top grid cell 0-10 m)  ocnBgchem: none  seaIce: SIS2
source_id: BCC-CSM2-MR
source_type: AOGCM
sub_experiment: none
sub_experiment_id: none
table_id: Amon
table_info: Creation Date:(30 July 2018) MD5:e53ff52009d0b97d9d867dc12b6096c7
title: BCC-CSM2-MR output prepared for CMIP6
tracking_id: hdl:21.14100/4f3fdd6c-bef7-4ec0-a692-5ca29f51e1ba
variable_id: rlut
variant_label: r1i1p1f1

&THISFILE_SPECIFICATIONS
    THISFILE_DATACOLUMNS = 11
    THISFILE_DATAROWS = 120
    THISFILE_FIRSTYEAR = 1850
    THISFILE_LASTYEAR = 1859
    THISFILE_ANNUALSTEPS = 12
    THISFILE_DATTYPE = 'MAG'
    THISFILE_REGIONMODE = 'NONE'
    THISFILE_TIMESERIESTYPE = 'MONTHLY'
    THISFILE_FIRSTDATAROW = 95
/

   VARIABLE                rlut                rlut                rlut                rlut                rlut                rlut                rlut                rlut                rlut                rlut                rlut
       TODO                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET
      UNITS           Wmsuper-2           Wmsuper-2           Wmsuper-2           Wmsuper-2           Wmsuper-2           Wmsuper-2           Wmsuper-2           Wmsuper-2           Wmsuper-2           Wmsuper-2           Wmsuper-2
      YEARS                 AMV                  NH                 N34             SHOCEAN              NHLAND                LAND              SHLAND               OCEAN                  SH               WORLD             NHOCEAN
1850.042000         2.44567e+02         2.31876e+02         2.73545e+02         2.45424e+02         2.17846e+02         2.23231e+02         2.34333e+02         2.43403e+02         2.43343e+02         2.37609e+02         2.40726e+02
1850.125000         2.47848e+02         2.36596e+02         2.76935e+02         2.44034e+02         2.22169e+02         2.23590e+02         2.26521e+02         2.44749e+02         2.40749e+02         2.38672e+02         2.45696e+02
1850.208000         2.47644e+02         2.38309e+02         2.80766e+02         2.42194e+02         2.25557e+02         2.25914e+02         2.26648e+02         2.43983e+02         2.39278e+02         2.38793e+02         2.46353e+02
1850.292000         2.55173e+02         2.43683e+02         2.82775e+02         2.40277e+02         2.36985e+02         2.31904e+02         2.21428e+02         2.43559e+02         2.36741e+02         2.40212e+02         2.47908e+02
1850.375000         2.56542e+02         2.44845e+02         2.66282e+02         2.37711e+02         2.41508e+02         2.35401e+02         2.22810e+02         2.41685e+02         2.34916e+02         2.39880e+02         2.46950e+02
1850.458000         2.56111e+02         2.47490e+02         2.71294e+02         2.38489e+02         2.50678e+02         2.42067e+02         2.24312e+02         2.41496e+02         2.35829e+02         2.41660e+02         2.45479e+02
1850.542000         2.59989e+02         2.49753e+02         2.83186e+02         2.40129e+02         2.56775e+02         2.46576e+02         2.25549e+02         2.42363e+02         2.37394e+02         2.43573e+02         2.45323e+02
1850.625000         2.57596e+02         2.48435e+02         2.83594e+02         2.39829e+02         2.53079e+02         2.43937e+02         2.25088e+02         2.42271e+02         2.37064e+02         2.42749e+02         2.45505e+02
1850.708000         2.51005e+02         2.43117e+02         2.83309e+02         2.42615e+02         2.44561e+02         2.39276e+02         2.28380e+02         2.42439e+02         2.39944e+02         2.41531e+02         2.42206e+02
1850.792000         2.47603e+02         2.38412e+02         2.82821e+02         2.43687e+02         2.33342e+02         2.32367e+02         2.30358e+02         2.42794e+02         2.41186e+02         2.39799e+02         2.41611e+02
1850.875000         2.43075e+02         2.34385e+02         2.84734e+02         2.44508e+02         2.25286e+02         2.27476e+02         2.31991e+02         2.42622e+02         2.42160e+02         2.38272e+02         2.40124e+02
1850.958000         2.45541e+02         2.32974e+02         2.80920e+02         2.43375e+02         2.19704e+02         2.25713e+02         2.38103e+02         2.42502e+02         2.42386e+02         2.37680e+02         2.41345e+02
1851.042000         2.44800e+02         2.31962e+02         2.84036e+02         2.45526e+02         2.16106e+02         2.20822e+02         2.30548e+02         2.43994e+02         2.42716e+02         2.37339e+02         2.41964e+02
1851.125000         2.49070e+02         2.37244e+02         2.79822e+02         2.42477e+02         2.21858e+02         2.23658e+02         2.27368e+02         2.44401e+02         2.39642e+02         2.38443e+02         2.46950e+02
1851.208000         2.50911e+02         2.39316e+02         2.81749e+02         2.41512e+02         2.27870e+02         2.26719e+02         2.24348e+02         2.43673e+02         2.38292e+02         2.38804e+02         2.46536e+02
1851.292000         2.53324e+02         2.40793e+02         2.80595e+02         2.39509e+02         2.32375e+02         2.29249e+02         2.22804e+02         2.42346e+02         2.36375e+02         2.38584e+02         2.46104e+02
1851.375000         2.54216e+02         2.43707e+02         2.77261e+02         2.38447e+02         2.41699e+02         2.35902e+02         2.23950e+02         2.41255e+02         2.35727e+02         2.39717e+02         2.44974e+02
1851.458000         2.55139e+02         2.47558e+02         2.78665e+02         2.38662e+02         2.52492e+02         2.42938e+02         2.23239e+02         2.41150e+02         2.35768e+02         2.41663e+02         2.44446e+02
1851.542000         2.56789e+02         2.50190e+02         2.81449e+02         2.38400e+02         2.56949e+02         2.45729e+02         2.22596e+02         2.41637e+02         2.35435e+02         2.42812e+02         2.45926e+02
1851.625000         2.56748e+02         2.48798e+02         2.87644e+02         2.40825e+02         2.53310e+02         2.44217e+02         2.25469e+02         2.43030e+02         2.37944e+02         2.43371e+02         2.45951e+02
1851.708000         2.50328e+02         2.44381e+02         2.85494e+02         2.40304e+02         2.46557e+02         2.40302e+02         2.27404e+02         2.41467e+02         2.37884e+02         2.41132e+02         2.43008e+02
1851.792000         2.47717e+02         2.38754e+02         2.83207e+02         2.43153e+02         2.32904e+02         2.30252e+02         2.24782e+02         2.42848e+02         2.39706e+02         2.39230e+02         2.42445e+02
1851.875000         2.45253e+02         2.33961e+02         2.85663e+02         2.44871e+02         2.24531e+02         2.26069e+02         2.29240e+02         2.42737e+02         2.41939e+02         2.37950e+02         2.39909e+02
1851.958000         2.44792e+02         2.31794e+02         2.89334e+02         2.45322e+02         2.17261e+02         2.22075e+02         2.32001e+02         2.43447e+02         2.42823e+02         2.37308e+02         2.40962e+02
1852.042000         2.46911e+02         2.31870e+02         2.81488e+02         2.43871e+02         2.16351e+02         2.21742e+02         2.32857e+02         2.42920e+02         2.41805e+02         2.36838e+02         2.41660e+02
1852.125000         2.48976e+02         2.35357e+02         2.79240e+02         2.43509e+02         2.19151e+02         2.21976e+02         2.27801e+02         2.44400e+02         2.40562e+02         2.37960e+02         2.45580e+02
1852.208000         2.49666e+02         2.38183e+02         2.81414e+02         2.42172e+02         2.27225e+02         2.25748e+02         2.22704e+02         2.43429e+02         2.38519e+02         2.38351e+02         2.45095e+02
1852.292000         2.54994e+02         2.42467e+02         2.78533e+02         2.38667e+02         2.35252e+02         2.31879e+02         2.24924e+02         2.42259e+02         2.36089e+02         2.39278e+02         2.47018e+02
1852.375000         2.56762e+02         2.44401e+02         2.76190e+02         2.38227e+02         2.42789e+02         2.37666e+02         2.27102e+02         2.41320e+02         2.36140e+02         2.40271e+02         2.45418e+02
1852.458000         2.55289e+02         2.47775e+02         2.76710e+02         2.37901e+02         2.52209e+02         2.42601e+02         2.22789e+02         2.40945e+02         2.35066e+02         2.41421e+02         2.44979e+02
1852.542000         2.59272e+02         2.49809e+02         2.76911e+02         2.39060e+02         2.56410e+02         2.45945e+02         2.24368e+02         2.41893e+02         2.36304e+02         2.43057e+02         2.45646e+02
1852.625000         2.60141e+02         2.48412e+02         2.79517e+02         2.39802e+02         2.53004e+02         2.43596e+02         2.24197e+02         2.42260e+02         2.36875e+02         2.42643e+02         2.45515e+02
1852.708000         2.53331e+02         2.43536e+02         2.78010e+02         2.42237e+02         2.44270e+02         2.38706e+02         2.27232e+02         2.42597e+02         2.39422e+02         2.41479e+02         2.43073e+02
1852.792000         2.45711e+02         2.38234e+02         2.80197e+02         2.44133e+02         2.34037e+02         2.32933e+02         2.30657e+02         2.42734e+02         2.41605e+02         2.39919e+02         2.40882e+02
1852.875000         2.42386e+02         2.32607e+02         2.85057e+02         2.45479e+02         2.23421e+02         2.25373e+02         2.29399e+02         2.42434e+02         2.42462e+02         2.37534e+02         2.38401e+02
1852.958000         2.41044e+02         2.30779e+02         2.75908e+02         2.45436e+02         2.17286e+02         2.22276e+02         2.32563e+02         2.42793e+02         2.43021e+02         2.36900e+02         2.39291e+02
1853.042000         2.45775e+02         2.33431e+02         2.74135e+02         2.44452e+02         2.19394e+02         2.24104e+02         2.33813e+02         2.43520e+02         2.42456e+02         2.37944e+02         2.42285e+02
1853.125000         2.46970e+02         2.36153e+02         2.69577e+02         2.42712e+02         2.20236e+02         2.22798e+02         2.28082e+02         2.44210e+02         2.39968e+02         2.38061e+02         2.46195e+02
1853.208000         2.48339e+02         2.39129e+02         2.72374e+02         2.41743e+02         2.26615e+02         2.26025e+02         2.24806e+02         2.44014e+02         2.38566e+02         2.38847e+02         2.47023e+02
1853.292000         2.53190e+02         2.43545e+02         2.62754e+02         2.37885e+02         2.36821e+02         2.32890e+02         2.24784e+02         2.42145e+02         2.35427e+02         2.39486e+02         2.47788e+02
1853.375000         2.52574e+02         2.44640e+02         2.68737e+02         2.38035e+02         2.42595e+02         2.36354e+02         2.23487e+02         2.41431e+02         2.35306e+02         2.39973e+02         2.45929e+02
1853.458000         2.54320e+02         2.47099e+02         2.76782e+02         2.38266e+02         2.50281e+02         2.40716e+02         2.20996e+02         2.41202e+02         2.35026e+02         2.41062e+02         2.45092e+02
1853.542000         2.58784e+02         2.50601e+02         2.85017e+02         2.39242e+02         2.59059e+02         2.48465e+02         2.26622e+02         2.41833e+02         2.36874e+02         2.43737e+02         2.45265e+02
1853.625000         2.56981e+02         2.49235e+02         2.84638e+02         2.39549e+02         2.55042e+02         2.43976e+02         2.21158e+02         2.42140e+02         2.36099e+02         2.42667e+02         2.45572e+02
1853.708000         2.50037e+02         2.43784e+02         2.83857e+02         2.41255e+02         2.44875e+02         2.37972e+02         2.23740e+02         2.42047e+02         2.37969e+02         2.40877e+02         2.43096e+02
1853.792000         2.48448e+02         2.38417e+02         2.87393e+02         2.43804e+02         2.34118e+02         2.32184e+02         2.28195e+02         2.42653e+02         2.40875e+02         2.39646e+02         2.41129e+02
1853.875000         2.43797e+02         2.34348e+02         2.79497e+02         2.44600e+02         2.26320e+02         2.28203e+02         2.32085e+02         2.42369e+02         2.42252e+02         2.38300e+02         2.39412e+02
1853.958000         2.44599e+02         2.31303e+02         2.79694e+02         2.44009e+02         2.17744e+02         2.23326e+02         2.34835e+02         2.42223e+02         2.42288e+02         2.36795e+02         2.39856e+02
1854.042000         2.49732e+02         2.33729e+02         2.71612e+02         2.44730e+02         2.19439e+02         2.23540e+02         2.31997e+02         2.43875e+02         2.42341e+02         2.38035e+02         2.42743e+02
1854.125000         2.48544e+02         2.34092e+02         2.76531e+02         2.42865e+02         2.18855e+02         2.21513e+02         2.26992e+02         2.43226e+02         2.39887e+02         2.36989e+02         2.43704e+02
1854.208000         2.50458e+02         2.39257e+02         2.78858e+02         2.42238e+02         2.28619e+02         2.25680e+02         2.19620e+02         2.43842e+02         2.37994e+02         2.38626e+02         2.45968e+02
1854.292000         2.52718e+02         2.41491e+02         2.79672e+02         2.39652e+02         2.35976e+02         2.32332e+02         2.24819e+02         2.41940e+02         2.36869e+02         2.39180e+02         2.44970e+02
1854.375000         2.55432e+02         2.44029e+02         2.75288e+02         2.38380e+02         2.41720e+02         2.35742e+02         2.23417e+02         2.41436e+02         2.35573e+02         2.39801e+02         2.45485e+02
1854.458000         2.55048e+02         2.47099e+02         2.77377e+02         2.37785e+02         2.50001e+02         2.41403e+02         2.23675e+02         2.41003e+02         2.35138e+02         2.41118e+02         2.45268e+02
1854.542000         2.58053e+02         2.50338e+02         2.85081e+02         2.37661e+02         2.57612e+02         2.46363e+02         2.23169e+02         2.41141e+02         2.34942e+02         2.42640e+02         2.45750e+02
1854.625000         2.58100e+02         2.47869e+02         2.84069e+02         2.39107e+02         2.50368e+02         2.41762e+02         2.24018e+02         2.42197e+02         2.36276e+02         2.42072e+02         2.46292e+02
1854.708000         2.52173e+02         2.43049e+02         2.83655e+02         2.40775e+02         2.43713e+02         2.38007e+02         2.26242e+02         2.41573e+02         2.38049e+02         2.40549e+02         2.42630e+02
1854.792000         2.46606e+02         2.39596e+02         2.83977e+02         2.42121e+02         2.35685e+02         2.33090e+02         2.27742e+02         2.42096e+02         2.39423e+02         2.39509e+02         2.42063e+02
1854.875000         2.44428e+02         2.33416e+02         2.85283e+02         2.43895e+02         2.24119e+02         2.26104e+02         2.30197e+02         2.41911e+02         2.41326e+02         2.37371e+02         2.39282e+02
1854.958000         2.44112e+02         2.31477e+02         2.83967e+02         2.45604e+02         2.18477e+02         2.22996e+02         2.32312e+02         2.43055e+02         2.43110e+02         2.37293e+02         2.39677e+02
1855.042000         2.47224e+02         2.32637e+02         2.78552e+02         2.45575e+02         2.18129e+02         2.22540e+02         2.31636e+02         2.43947e+02         2.42960e+02         2.37799e+02         2.41790e+02
1855.125000         2.44995e+02         2.33825e+02         2.70790e+02         2.44440e+02         2.20304e+02         2.21903e+02         2.25201e+02         2.43543e+02         2.40831e+02         2.37328e+02         2.42354e+02
1855.208000         2.51470e+02         2.37502e+02         2.82070e+02         2.42088e+02         2.26993e+02         2.25064e+02         2.21085e+02         2.42967e+02         2.38148e+02         2.37825e+02         2.44132e+02
1855.292000         2.54912e+02         2.41960e+02         2.77632e+02         2.38658e+02         2.36621e+02         2.31833e+02         2.21961e+02         2.41527e+02         2.35525e+02         2.38743e+02         2.45328e+02
1855.375000         2.55618e+02         2.44542e+02         2.75349e+02         2.38119e+02         2.42181e+02         2.35297e+02         2.21103e+02         2.41522e+02         2.34927e+02         2.39734e+02         2.46030e+02
1855.458000         2.57404e+02         2.48492e+02         2.72313e+02         2.37686e+02         2.51645e+02         2.42120e+02         2.22481e+02         2.41478e+02         2.34833e+02         2.41662e+02         2.46502e+02
1855.542000         2.58700e+02         2.49696e+02         2.82217e+02         2.39368e+02         2.55857e+02         2.45598e+02         2.24445e+02         2.42139e+02         2.36568e+02         2.43132e+02         2.45809e+02
1855.625000         2.57123e+02         2.48214e+02         2.83450e+02         2.39886e+02         2.51078e+02         2.42159e+02         2.23768e+02         2.42691e+02         2.36862e+02         2.42538e+02         2.46408e+02
1855.708000         2.53449e+02         2.42777e+02         2.85018e+02         2.42179e+02         2.44351e+02         2.39217e+02         2.28632e+02         2.42009e+02         2.39637e+02         2.41207e+02         2.41784e+02
1855.792000         2.47781e+02         2.38186e+02         2.81692e+02         2.43021e+02         2.32602e+02         2.31053e+02         2.27860e+02         2.42456e+02         2.40177e+02         2.39181e+02         2.41708e+02
1855.875000         2.43044e+02         2.33629e+02         2.84011e+02         2.44754e+02         2.25144e+02         2.27541e+02         2.32485e+02         2.42271e+02         2.42452e+02         2.38041e+02         2.38981e+02
1855.958000         2.45717e+02         2.33610e+02         2.81740e+02         2.45114e+02         2.21788e+02         2.26875e+02         2.37364e+02         2.43373e+02         2.43660e+02         2.38635e+02         2.41067e+02
1856.042000         2.46823e+02         2.34147e+02         2.72430e+02         2.43619e+02         2.20605e+02         2.25542e+02         2.35720e+02         2.43219e+02         2.42137e+02         2.38142e+02         2.42690e+02
1856.125000         2.48188e+02         2.35029e+02         2.72981e+02         2.43148e+02         2.19205e+02         2.22927e+02         2.30601e+02         2.43950e+02         2.40794e+02         2.37912e+02         2.45012e+02
1856.208000         2.49344e+02         2.39723e+02         2.74051e+02         2.41175e+02         2.27146e+02         2.25439e+02         2.21920e+02         2.43963e+02         2.37562e+02         2.38643e+02         2.47657e+02
1856.292000         2.53369e+02         2.41482e+02         2.75644e+02         2.38067e+02         2.33278e+02         2.29755e+02         2.22492e+02         2.41762e+02         2.35145e+02         2.38313e+02         2.46657e+02
1856.375000         2.53051e+02         2.44245e+02         2.69356e+02         2.37421e+02         2.42347e+02         2.35871e+02         2.22519e+02         2.40871e+02         2.34625e+02         2.39435e+02         2.45442e+02
1856.458000         2.56682e+02         2.47309e+02         2.70386e+02         2.38014e+02         2.50757e+02         2.40988e+02         2.20845e+02         2.41076e+02         2.34793e+02         2.41051e+02         2.45134e+02
1856.542000         2.58975e+02         2.49257e+02         2.78077e+02         2.38214e+02         2.55696e+02         2.45264e+02         2.23757e+02         2.41217e+02         2.35502e+02         2.42379e+02         2.45196e+02
1856.625000         2.57135e+02         2.47439e+02         2.80462e+02         2.40769e+02         2.52126e+02         2.43161e+02         2.24676e+02         2.42367e+02         2.37750e+02         2.42595e+02         2.44483e+02
1856.708000         2.47086e+02         2.43163e+02         2.78391e+02         2.41661e+02         2.44436e+02         2.38989e+02         2.27758e+02         2.41962e+02         2.39053e+02         2.41108e+02         2.42360e+02
1856.792000         2.45509e+02         2.37799e+02         2.88558e+02         2.44278e+02         2.31970e+02         2.32059e+02         2.32241e+02         2.43073e+02         2.42019e+02         2.39909e+02         2.41476e+02
1856.875000         2.45940e+02         2.34968e+02         2.81926e+02         2.44317e+02         2.24157e+02         2.26453e+02         2.31186e+02         2.43229e+02         2.41853e+02         2.38411e+02         2.41788e+02
1856.958000         2.45041e+02         2.31607e+02         2.73787e+02         2.45154e+02         2.17051e+02         2.23421e+02         2.36554e+02         2.43277e+02         2.43541e+02         2.37574e+02         2.40789e+02
1857.042000         2.48083e+02         2.32909e+02         2.73255e+02         2.44312e+02         2.16943e+02         2.21776e+02         2.31740e+02         2.43739e+02         2.41953e+02         2.37431e+02         2.42980e+02
1857.125000         2.48540e+02         2.35579e+02         2.71637e+02         2.43076e+02         2.20892e+02         2.23501e+02         2.28880e+02         2.43836e+02         2.40413e+02         2.37996e+02         2.44843e+02
1857.208000         2.48182e+02         2.40081e+02         2.76738e+02         2.41790e+02         2.27829e+02         2.26396e+02         2.23440e+02         2.44380e+02         2.38348e+02         2.39214e+02         2.47810e+02
1857.292000         2.52153e+02         2.42765e+02         2.77642e+02         2.38797e+02         2.36065e+02         2.32105e+02         2.23941e+02         2.42322e+02         2.36010e+02         2.39388e+02         2.46991e+02
1857.375000         2.53713e+02         2.44845e+02         2.69684e+02         2.38436e+02         2.44132e+02         2.37445e+02         2.23657e+02         2.41386e+02         2.35663e+02         2.40254e+02         2.45295e+02
1857.458000         2.54128e+02         2.46965e+02         2.75481e+02         2.38686e+02         2.52048e+02         2.41398e+02         2.19441e+02         2.40867e+02         2.35075e+02         2.41020e+02         2.43758e+02
1857.542000         2.58963e+02         2.50357e+02         2.82777e+02         2.40069e+02         2.57336e+02         2.46332e+02         2.23645e+02         2.42600e+02         2.36988e+02         2.43672e+02         2.45954e+02
1857.625000         2.59162e+02         2.47840e+02         2.84296e+02         2.40868e+02         2.52280e+02         2.44156e+02         2.27406e+02         2.42662e+02         2.38342e+02         2.43091e+02         2.45040e+02
1857.708000         2.50488e+02         2.43541e+02         2.79958e+02         2.42827e+02         2.44052e+02         2.38129e+02         2.25918e+02         2.42995e+02         2.39655e+02         2.41598e+02         2.43218e+02
1857.792000         2.45555e+02         2.37842e+02         2.80954e+02         2.43857e+02         2.33537e+02         2.31856e+02         2.28388e+02         2.42438e+02         2.40955e+02         2.39398e+02         2.40558e+02
1857.875000         2.44676e+02         2.33695e+02         2.80440e+02         2.43957e+02         2.23006e+02         2.26299e+02         2.33090e+02         2.42444e+02         2.41919e+02         2.37807e+02         2.40439e+02
1857.958000         2.47381e+02         2.31546e+02         2.78583e+02         2.45574e+02         2.17118e+02         2.22832e+02         2.34612e+02         2.43455e+02         2.43517e+02         2.37532e+02         2.40647e+02
1858.042000         2.45511e+02         2.33535e+02         2.80025e+02         2.44550e+02         2.16612e+02         2.21506e+02         2.31596e+02         2.44404e+02         2.42120e+02         2.37828e+02         2.44211e+02
1858.125000         2.48801e+02         2.35863e+02         2.74571e+02         2.43290e+02         2.20007e+02         2.22889e+02         2.28832e+02         2.44398e+02         2.40577e+02         2.38220e+02         2.45866e+02
1858.208000         2.51329e+02         2.40662e+02         2.74468e+02         2.40646e+02         2.29488e+02         2.28128e+02         2.25323e+02         2.43685e+02         2.37772e+02         2.39217e+02         2.47711e+02
1858.292000         2.53978e+02         2.41824e+02         2.78038e+02         2.38962e+02         2.34241e+02         2.31264e+02         2.25128e+02         2.42250e+02         2.36366e+02         2.39095e+02         2.46608e+02
1858.375000         2.54948e+02         2.44788e+02         2.69361e+02         2.37675e+02         2.44633e+02         2.38078e+02         2.24562e+02         2.40777e+02         2.35215e+02         2.40001e+02         2.44886e+02
1858.458000         2.56883e+02         2.48234e+02         2.75877e+02         2.39589e+02         2.52258e+02         2.43044e+02         2.24048e+02         2.42216e+02         2.36673e+02         2.42454e+02         2.45695e+02
1858.542000         2.59691e+02         2.50727e+02         2.83146e+02         2.39150e+02         2.58368e+02         2.46617e+02         2.22389e+02         2.42056e+02         2.36005e+02         2.43366e+02         2.45907e+02
1858.625000         2.56987e+02         2.49424e+02         2.83476e+02         2.39775e+02         2.55758e+02         2.46120e+02         2.26247e+02         2.42207e+02         2.37238e+02         2.43331e+02         2.45429e+02
1858.708000         2.51837e+02         2.44023e+02         2.88218e+02         2.41430e+02         2.45363e+02         2.40261e+02         2.29740e+02         2.42181e+02         2.39237e+02         2.41630e+02         2.43177e+02
1858.792000         2.46529e+02         2.38768e+02         2.87745e+02         2.43000e+02         2.34416e+02         2.32792e+02         2.29443e+02         2.42361e+02         2.40457e+02         2.39613e+02         2.41514e+02
1858.875000         2.43103e+02         2.35144e+02         2.83261e+02         2.43503e+02         2.25143e+02         2.27008e+02         2.30855e+02         2.42621e+02         2.41130e+02         2.38137e+02         2.41452e+02
1858.958000         2.42761e+02         2.32766e+02         2.78440e+02         2.43434e+02         2.19792e+02         2.24600e+02         2.34513e+02         2.42366e+02         2.41761e+02         2.37264e+02         2.40951e+02
1859.042000         2.46722e+02         2.33288e+02         2.77218e+02         2.44441e+02         2.17475e+02         2.22772e+02         2.33694e+02         2.43934e+02         2.42425e+02         2.37856e+02         2.43263e+02
1859.125000         2.48610e+02         2.35727e+02         2.78986e+02         2.43553e+02         2.19686e+02         2.21971e+02         2.26682e+02         2.44540e+02         2.40388e+02         2.38058e+02         2.45847e+02
1859.208000         2.47882e+02         2.38067e+02         2.80150e+02         2.42014e+02         2.25978e+02         2.24829e+02         2.22459e+02         2.43597e+02         2.38345e+02         2.38206e+02         2.45694e+02
1859.292000         2.52602e+02         2.41035e+02         2.78789e+02         2.39799e+02         2.35115e+02         2.31194e+02         2.23108e+02         2.41937e+02         2.36668e+02         2.38851e+02         2.44769e+02
1859.375000         2.53088e+02         2.44196e+02         2.70241e+02         2.38897e+02         2.44119e+02         2.38154e+02         2.25855e+02         2.41197e+02         2.36450e+02         2.40323e+02         2.44245e+02
1859.458000         2.56743e+02         2.47926e+02         2.71864e+02         2.38546e+02         2.53053e+02         2.42774e+02         2.21580e+02         2.41189e+02         2.35363e+02         2.41644e+02         2.44692e+02
1859.542000         2.55915e+02         2.49862e+02         2.75371e+02         2.40139e+02         2.58775e+02         2.47320e+02         2.23702e+02         2.41903e+02         2.37056e+02         2.43459e+02         2.44239e+02
1859.625000         2.56622e+02         2.48397e+02         2.84160e+02         2.40742e+02         2.54052e+02         2.45368e+02         2.27465e+02         2.42500e+02         2.38251e+02         2.43324e+02         2.44830e+02
1859.708000         2.53767e+02         2.43290e+02         2.81796e+02         2.42179e+02         2.43857e+02         2.38750e+02         2.28220e+02         2.42503e+02         2.39560e+02         2.41425e+02         2.42933e+02
1859.792000         2.47200e+02         2.39161e+02         2.81836e+02         2.43213e+02         2.34738e+02         2.32501e+02         2.27888e+02         2.42670e+02         2.40338e+02         2.39749e+02         2.41950e+02
1859.875000         2.42912e+02         2.33256e+02         2.75618e+02         2.44556e+02         2.22530e+02         2.25610e+02         2.31962e+02         2.42606e+02         2.42193e+02         2.37725e+02         2.40023e+02
1859.958000         2.44778e+02         2.30236e+02         2.72090e+02         2.44966e+02         2.15445e+02         2.21495e+02         2.33968e+02         2.42644e+02         2.42903e+02         2.36569e+02         2.39567e+02

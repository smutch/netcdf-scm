---- HEADER ----

Date: 2019-11-12 08:39:04
Contact: cmip6output wrangling regression test
Source data crunched with: NetCDF-SCM v0+untagged.12.g4182656 (more info at gitlab.com/znicholls/netcdf-scm)
File written with: pymagicc v2.0.0-beta.8 (more info at github.com/openclimatedata/pymagicc)


---- METADATA ----

CDI: Climate Data Interface version 1.8.2 (http://mpimet.mpg.de/cdi)
CDO: Climate Data Operators version 1.8.2 (http://mpimet.mpg.de/cdo)
CMIP6_CV_version: cv=6.2.3.0-7-g2019642
Conventions: CF-1.5
EXPID: CNRM-CM6-1_historical_r1i1p1f2
NCO: "4.5.5"
area_world (m**2): 146378816176539.2
area_world_land (m**2): 146378816176539.2
area_world_northern_hemisphere (m**2): 98545925115982.06
area_world_northern_hemisphere_land (m**2): 98545924857845.0
area_world_southern_hemisphere (m**2): 47832891462226.71
area_world_southern_hemisphere_land (m**2): 47832891318694.2
arpege_minor_version: 6.3.2
branch_method: standard
branch_time_in_child: 0.0
branch_time_in_parent: 0.0
calendar: gregorian
contact: contact.cmip@meteo.fr
creation_date: 2018-06-20T08:40:04Z
crunch_contact: cmip6output crunching regression test
crunch_netcdf_scm_version: 0+untagged.12.g4182656 (more info at gitlab.com/znicholls/netcdf-scm)
crunch_source_files: Files: ['/CMIP6/CMIP/CNRM-CERFACS/CNRM-CM6-1/historical/r1i1p1f2/Lmon/lai/gr/v20180917/lai_Lmon_CNRM-CM6-1_historical_r1i1p1f2_gr_200201-200512.nc']
data_specs_version: 01.00.21
description: a ratio obtained by dividing the total upper leaf surface area of vegetation by the (horizontal) surface area of the land on which it grows.
dr2xml_md5sum: d6225e658d7de0912fca2a4293dbe2a7
dr2xml_version: 1.10
experiment: all-forcing simulation of the recent past
experiment_id: historical
external_variables: areacella
forcing_index: 2
frequency: mon
further_info_url: https://furtherinfo.es-doc.org/CMIP6.CNRM-CERFACS.CNRM-CM6-1.historical.none.r1i1p1f2
grid: data regridded to a T127 gaussian grid (128x256 latlon) from a native atmosphere T127l reduced gaussian grid
grid_label: gr
history: none
initialization_index: 1
institution: CNRM (Centre National de Recherches Meteorologiques, Toulouse 31057, France), CERFACS (Centre Europeen de Recherche et de Formation Avancee en Calcul Scientifique, Toulouse 31057, France)
institution_id: CNRM-CERFACS
interval_operation: 900 s
interval_write: 1 month
license: CMIP6 model data produced by CNRM-CERFACS is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License (https://creativecommons.org/licenses). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file) and at http://www.umr-cnrm.fr/cmip6/. The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.
name: /scratch/work/voldoire/outputs/CMIP6/DECK/CNRM-CM6-1_historical_r1i1p1f2/18500101/lai_Lmon_CNRM-CM6-1_historical_r1i1p1f2_gr_%start_date%-%end_date%
nemo_gelato_commit: 49095b3accd5d4c_6524fe19b00467a
netcdf-scm crunched file: CMIP6/CMIP/CNRM-CERFACS/CNRM-CM6-1/historical/r1i1p1f2/Lmon/lai/gr/v20180917/netcdf-scm_lai_Lmon_CNRM-CM6-1_historical_r1i1p1f2_gr_200201-200512.nc
nominal_resolution: 250 km
online_operation: average
parent_activity_id: CMIP
parent_experiment_id: piControl
parent_mip_era: CMIP6
parent_source_id: CNRM-CM6-1
parent_time_units: days since 1850-01-01 00:00:00
parent_variant_label: r1i1p1f2
physics_index: 1
product: model-output
realization_index: 1
realm: land
references: http://www.umr-cnrm.fr/cmip6/references
source: CNRM-CM6-1 (2017):  aerosol: prescribed monthly fields computed by TACTIC_v2 scheme atmos: Arpege 6.3 (T127; Gaussian Reduced with 24572 grid points in total distributed over 128 latitude circles (with 256 grid points per latitude circle between 30degN and 30degS reducing to 20 grid points per latitude circle at 88.9degN and 88.9degS); 91 levels; top level 78.4 km) atmosChem: OZL_v2 land: Surfex 8.0c ocean: Nemo 3.6 (eORCA1, tripolar primarily 1deg; 362 x 294 longitude/latitude; 75 levels; top grid cell 0-1 m) seaIce: Gelato 6.1
source_id: CNRM-CM6-1
source_type: AOGCM
sub_experiment: none
sub_experiment_id: none
table_id: Lmon
title: CNRM-CM6-1 model output prepared for CMIP6 / CMIP historical
tracking_id: hdl:21.14100/40b9f459-57cb-4f7f-a796-1effe4ac6624
variable_id: lai
variant_label: r1i1p1f2
xios_commit: 1442-shuffle

&THISFILE_SPECIFICATIONS
    THISFILE_DATACOLUMNS = 6
    THISFILE_DATAROWS = 48
    THISFILE_FIRSTYEAR = 2002
    THISFILE_LASTYEAR = 2005
    THISFILE_ANNUALSTEPS = 12
    THISFILE_DATTYPE = 'MAG'
    THISFILE_REGIONMODE = 'NONE'
    THISFILE_TIMESERIESTYPE = 'MONTHLY'
    THISFILE_FIRSTDATAROW = 96
/

   VARIABLE                 lai                 lai                 lai                 lai                 lai                 lai
       TODO                 SET                 SET                 SET                 SET                 SET                 SET
      UNITS       dimensionless       dimensionless       dimensionless       dimensionless       dimensionless       dimensionless
      YEARS              SHLAND                  SH               WORLD                  NH                LAND              NHLAND
2002.042000         1.88686e+00         1.88686e+00         1.19827e+00         8.66569e-01         1.19827e+00         8.66569e-01
2002.125000         1.89760e+00         1.89760e+00         1.21725e+00         8.89519e-01         1.21725e+00         8.89519e-01
2002.208000         1.89718e+00         1.89718e+00         1.26800e+00         9.64908e-01         1.26800e+00         9.64908e-01
2002.292000         1.86497e+00         1.86497e+00         1.38586e+00         1.15506e+00         1.38586e+00         1.15506e+00
2002.375000         1.83532e+00         1.83532e+00         1.67025e+00         1.59074e+00         1.67025e+00         1.59074e+00
2002.458000         1.74805e+00         1.74805e+00         1.87495e+00         1.93608e+00         1.87495e+00         1.93608e+00
2002.542000         1.66907e+00         1.66907e+00         1.96802e+00         2.11203e+00         1.96802e+00         2.11203e+00
2002.625000         1.59771e+00         1.59771e+00         1.91118e+00         2.06218e+00         1.91118e+00         2.06218e+00
2002.708000         1.59260e+00         1.59260e+00         1.77752e+00         1.86660e+00         1.77752e+00         1.86660e+00
2002.792000         1.66590e+00         1.66590e+00         1.55801e+00         1.50603e+00         1.55801e+00         1.50603e+00
2002.875000         1.76604e+00         1.76604e+00         1.35041e+00         1.15019e+00         1.35041e+00         1.15019e+00
2002.958000         1.81914e+00         1.81914e+00         1.23693e+00         9.56472e-01         1.23693e+00         9.56472e-01
2003.042000         1.88686e+00         1.88686e+00         1.19827e+00         8.66569e-01         1.19827e+00         8.66569e-01
2003.125000         1.89760e+00         1.89760e+00         1.21725e+00         8.89519e-01         1.21725e+00         8.89519e-01
2003.208000         1.89718e+00         1.89718e+00         1.26800e+00         9.64908e-01         1.26800e+00         9.64908e-01
2003.292000         1.86497e+00         1.86497e+00         1.38586e+00         1.15506e+00         1.38586e+00         1.15506e+00
2003.375000         1.83532e+00         1.83532e+00         1.67025e+00         1.59074e+00         1.67025e+00         1.59074e+00
2003.458000         1.74805e+00         1.74805e+00         1.87495e+00         1.93608e+00         1.87495e+00         1.93608e+00
2003.542000         1.66907e+00         1.66907e+00         1.96802e+00         2.11203e+00         1.96802e+00         2.11203e+00
2003.625000         1.59771e+00         1.59771e+00         1.91118e+00         2.06218e+00         1.91118e+00         2.06218e+00
2003.708000         1.59260e+00         1.59260e+00         1.77752e+00         1.86660e+00         1.77752e+00         1.86660e+00
2003.792000         1.66590e+00         1.66590e+00         1.55801e+00         1.50603e+00         1.55801e+00         1.50603e+00
2003.875000         1.76604e+00         1.76604e+00         1.35041e+00         1.15019e+00         1.35041e+00         1.15019e+00
2003.958000         1.81914e+00         1.81914e+00         1.23693e+00         9.56472e-01         1.23693e+00         9.56472e-01
2004.042000         1.88686e+00         1.88686e+00         1.19827e+00         8.66569e-01         1.19827e+00         8.66569e-01
2004.125000         1.89770e+00         1.89770e+00         1.21771e+00         8.90150e-01         1.21771e+00         8.90150e-01
2004.208000         1.89718e+00         1.89718e+00         1.26800e+00         9.64908e-01         1.26800e+00         9.64908e-01
2004.292000         1.86497e+00         1.86497e+00         1.38586e+00         1.15506e+00         1.38586e+00         1.15506e+00
2004.375000         1.83532e+00         1.83532e+00         1.67025e+00         1.59074e+00         1.67025e+00         1.59074e+00
2004.458000         1.74805e+00         1.74805e+00         1.87495e+00         1.93608e+00         1.87495e+00         1.93608e+00
2004.542000         1.66907e+00         1.66907e+00         1.96802e+00         2.11203e+00         1.96802e+00         2.11203e+00
2004.625000         1.59771e+00         1.59771e+00         1.91118e+00         2.06218e+00         1.91118e+00         2.06218e+00
2004.708000         1.59260e+00         1.59260e+00         1.77752e+00         1.86660e+00         1.77752e+00         1.86660e+00
2004.792000         1.66590e+00         1.66590e+00         1.55801e+00         1.50603e+00         1.55801e+00         1.50603e+00
2004.875000         1.76604e+00         1.76604e+00         1.35041e+00         1.15019e+00         1.35041e+00         1.15019e+00
2004.958000         1.81914e+00         1.81914e+00         1.23693e+00         9.56472e-01         1.23693e+00         9.56472e-01
2005.042000         1.88686e+00         1.88686e+00         1.19827e+00         8.66569e-01         1.19827e+00         8.66569e-01
2005.125000         1.89760e+00         1.89760e+00         1.21725e+00         8.89519e-01         1.21725e+00         8.89519e-01
2005.208000         1.89718e+00         1.89718e+00         1.26800e+00         9.64908e-01         1.26800e+00         9.64908e-01
2005.292000         1.86497e+00         1.86497e+00         1.38586e+00         1.15506e+00         1.38586e+00         1.15506e+00
2005.375000         1.83532e+00         1.83532e+00         1.67025e+00         1.59074e+00         1.67025e+00         1.59074e+00
2005.458000         1.74805e+00         1.74805e+00         1.87495e+00         1.93608e+00         1.87495e+00         1.93608e+00
2005.542000         1.66907e+00         1.66907e+00         1.96802e+00         2.11203e+00         1.96802e+00         2.11203e+00
2005.625000         1.59771e+00         1.59771e+00         1.91118e+00         2.06218e+00         1.91118e+00         2.06218e+00
2005.708000         1.59260e+00         1.59260e+00         1.77752e+00         1.86660e+00         1.77752e+00         1.86660e+00
2005.792000         1.66590e+00         1.66590e+00         1.55801e+00         1.50603e+00         1.55801e+00         1.50603e+00
2005.875000         1.76604e+00         1.76604e+00         1.35041e+00         1.15019e+00         1.35041e+00         1.15019e+00
2005.958000         1.81914e+00         1.81914e+00         1.23693e+00         9.56472e-01         1.23693e+00         9.56472e-01

---- HEADER ----

Date: 2019-11-12 08:41:53
Contact: cmip6output wrangling regression test
Source data crunched with: NetCDF-SCM v0+untagged.12.g4182656 (more info at gitlab.com/znicholls/netcdf-scm)
File written with: pymagicc v2.0.0-beta.8 (more info at github.com/openclimatedata/pymagicc)


---- METADATA ----

CDI: Climate Data Interface version 1.8.2 (http://mpimet.mpg.de/cdi)
CDO: Climate Data Operators version 1.8.2 (http://mpimet.mpg.de/cdo)
Conventions: CF-1.5
area_world (m**2): 509499402446956.25
area_world_el_nino_n3.4 (m**2): 7396831885217.78
area_world_land (m**2): 146406001767335.12
area_world_north_atlantic_ocean (m**2): 39659751344075.11
area_world_northern_hemisphere (m**2): 254749701223478.12
area_world_northern_hemisphere_land (m**2): 98588705990809.72
area_world_northern_hemisphere_ocean (m**2): 156160995225729.12
area_world_ocean (m**2): 363093400684925.3
area_world_southern_hemisphere (m**2): 254749701223478.1
area_world_southern_hemisphere_land (m**2): 47817295776525.4
area_world_southern_hemisphere_ocean (m**2): 206932405459196.22
branch_method: standard
branch_time_in_child: 0.0
branch_time_in_parent: 0.0
calendar: 365_day
cmor_version: 3.3.2
comment: near-surface (usually, 2 meter) air temperature
contact: Kenneth Lo (cdkkl@giss.nasa.gov)
creation_date: 2018-10-03T00:27:54Z
crunch_contact: cmip6output crunching regression test
crunch_netcdf_scm_version: 0+untagged.12.g4182656 (more info at gitlab.com/znicholls/netcdf-scm)
crunch_source_files: Files: ['/CMIP6/CMIP/NASA-GISS/GISS-E2-1-G/abrupt-4xCO2/r1i1p1f1/Amon/tas/gn/v20181002/tas_Amon_GISS-E2-1-G_abrupt-4xCO2_r1i1p1f1_gn_185001-185412.nc']
data_specs_version: 01.00.23
experiment: abrupt quadrupling of CO2
experiment_id: abrupt-4xCO2
external_variables: areacella
forcing_index: 1
frequency: mon
further_info_url: https://furtherinfo.es-doc.org/CMIP6.NASA-GISS.GISS-E2-1-G.abrupt-4xCO2.none.r1i1p1f1
grid: atmospheric grid: 144x90, ocean grid: 288x180
grid_label: gn
history: 2018-10-03T00:27:54Z altered by CMOR: Treated scalar dimension: 'height'. 2018-10-03T00:27:54Z altered by CMOR: replaced missing value flag (-1e+30) with standard missing value (1e+20).
initialization_index: 1
institution: Goddard Institute for Space Studies, New York, NY 10025, USA
institution_id: NASA-GISS
land_fraction: 0.2873526466649338
land_fraction_northern_hemisphere: 0.3870022438390347
land_fraction_southern_hemisphere: 0.18770304949083289
license: CMIP6 model data produced by NASA Goddard Institute for Space Studies is licensed under a Creative Commons Attribution ShareAlike 4.0 International License (https://creativecommons.org/licenses). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file) and at https:///pcmdi.llnl.gov/. The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.
model_id: E200_4xCO2aF40oQ40_2.json
nominal_resolution: 250 km
parent_activity_id: CMIP
parent_experiment_id: piControl
parent_experiment_rip: r1i1p1
parent_mip_era: CMIP6
parent_source_id: GISS-E2-1-G
parent_time_units: days since 4150-1-1
parent_variant_label: r1i1p1f1
physics_index: 1
product: model-output
realization_index: 1
realm: atmos
references: https://data.giss.nasa.gov/modelE/cmip6
source: GISS-E2.1G (2016): 
aerosol: Varies with physics-version (p==1 none, p==3 OMA, p==4 TOMAS, p==5 MATRIX)
atmos: GISS-E2.1 (2.5x2 degree; 144 x 90 longitude/latitude; 40 levels; top level 0.1 hPa)
atmosChem: Varies with physics-version (p==1 Non-interactive, p>1 GPUCCINI)
land: GISS LSM
landIce: none
ocean: GISS Ocean (1.25x1 degree; 288 x 180 longitude/latitude; 32 levels; top grid cell 0-10 m)
ocnBgchem: none
seaIce: GISS SI
source_id: GISS-E2-1-G
source_type: AOGCM
sub_experiment: none
sub_experiment_id: none
table_id: Amon
table_info: Creation Date:(21 March 2018) MD5:f76dbc1e8bf6b7e4aee30573a09e5454
title: GISS-E2-1-G output prepared for CMIP6
tracking_id: hdl:21.14100/5a56a8c4-c09a-4e5c-ae53-fd66055789fa
variable_id: tas
variant_label: r1i1p1f1

&THISFILE_SPECIFICATIONS
    THISFILE_DATACOLUMNS = 11
    THISFILE_DATAROWS = 60
    THISFILE_FIRSTYEAR = 1850
    THISFILE_LASTYEAR = 1854
    THISFILE_ANNUALSTEPS = 12
    THISFILE_DATTYPE = 'MAG'
    THISFILE_REGIONMODE = 'NONE'
    THISFILE_TIMESERIESTYPE = 'MONTHLY'
    THISFILE_FIRSTDATAROW = 103
/

   VARIABLE                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas
       TODO                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET
      UNITS                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K
      YEARS               WORLD                 N34                LAND                 AMV                  NH              NHLAND             NHOCEAN               OCEAN                  SH              SHLAND             SHOCEAN
1850.042000         2.85643e+02         2.99958e+02         2.76813e+02         2.89057e+02         2.81162e+02         2.72474e+02         2.86646e+02         2.89203e+02         2.90123e+02         2.85758e+02         2.91132e+02
1850.125000         2.86140e+02         3.00294e+02         2.77666e+02         2.89027e+02         2.81855e+02         2.74377e+02         2.86575e+02         2.89557e+02         2.90425e+02         2.84447e+02         2.91806e+02
1850.208000         2.86849e+02         3.00616e+02         2.79538e+02         2.89245e+02         2.84014e+02         2.78775e+02         2.87321e+02         2.89798e+02         2.89685e+02         2.81110e+02         2.91666e+02
1850.292000         2.87727e+02         3.00735e+02         2.82040e+02         2.90080e+02         2.86918e+02         2.83615e+02         2.89002e+02         2.90019e+02         2.88536e+02         2.78794e+02         2.90787e+02
1850.375000         2.88747e+02         3.01106e+02         2.84900e+02         2.91726e+02         2.90322e+02         2.88778e+02         2.91297e+02         2.90298e+02         2.87171e+02         2.76905e+02         2.89543e+02
1850.458000         2.89525e+02         3.01447e+02         2.87208e+02         2.93574e+02         2.93229e+02         2.93204e+02         2.93244e+02         2.90459e+02         2.85822e+02         2.74846e+02         2.88358e+02
1850.542000         2.90073e+02         3.01086e+02         2.88914e+02         2.95074e+02         2.95074e+02         2.95872e+02         2.94571e+02         2.90540e+02         2.85071e+02         2.74568e+02         2.87498e+02
1850.625000         2.89868e+02         3.00741e+02         2.88380e+02         2.95725e+02         2.95057e+02         2.94964e+02         2.95116e+02         2.90468e+02         2.84679e+02         2.74803e+02         2.86961e+02
1850.708000         2.89180e+02         3.00803e+02         2.86416e+02         2.95660e+02         2.93255e+02         2.91256e+02         2.94517e+02         2.90295e+02         2.85106e+02         2.76436e+02         2.87109e+02
1850.792000         2.88228e+02         3.01403e+02         2.83540e+02         2.94336e+02         2.89960e+02         2.85361e+02         2.92863e+02         2.90119e+02         2.86497e+02         2.79787e+02         2.88048e+02
1850.875000         2.87612e+02         3.01850e+02         2.81545e+02         2.92705e+02         2.86859e+02         2.80413e+02         2.90928e+02         2.90059e+02         2.88366e+02         2.83878e+02         2.89403e+02
1850.958000         2.86820e+02         3.02060e+02         2.79149e+02         2.91544e+02         2.83632e+02         2.75612e+02         2.88695e+02         2.89913e+02         2.90009e+02         2.86443e+02         2.90833e+02
1851.042000         2.86617e+02         3.01999e+02         2.77705e+02         2.90461e+02         2.82170e+02         2.73282e+02         2.87780e+02         2.90211e+02         2.91065e+02         2.86824e+02         2.92045e+02
1851.125000         2.87332e+02         3.02081e+02         2.79722e+02         2.90114e+02         2.83161e+02         2.76391e+02         2.87435e+02         2.90401e+02         2.91504e+02         2.86591e+02         2.92639e+02
1851.208000         2.87861e+02         3.02199e+02         2.80954e+02         2.90172e+02         2.85023e+02         2.80023e+02         2.88179e+02         2.90646e+02         2.90699e+02         2.82873e+02         2.92507e+02
1851.292000         2.88913e+02         3.02264e+02         2.83807e+02         2.91056e+02         2.88265e+02         2.85517e+02         2.90000e+02         2.90972e+02         2.89560e+02         2.80279e+02         2.91705e+02
1851.375000         2.89863e+02         3.02328e+02         2.86319e+02         2.92515e+02         2.91568e+02         2.90353e+02         2.92336e+02         2.91293e+02         2.88158e+02         2.78002e+02         2.90505e+02
1851.458000         2.90666e+02         3.02514e+02         2.89065e+02         2.94151e+02         2.94519e+02         2.95087e+02         2.94161e+02         2.91311e+02         2.86812e+02         2.76649e+02         2.89160e+02
1851.542000         2.90860e+02         3.01344e+02         2.89927e+02         2.95559e+02         2.96000e+02         2.97056e+02         2.95333e+02         2.91236e+02         2.85719e+02         2.75227e+02         2.88144e+02
1851.625000         2.90674e+02         3.00617e+02         2.89506e+02         2.96287e+02         2.95893e+02         2.96037e+02         2.95803e+02         2.91146e+02         2.85455e+02         2.76038e+02         2.87631e+02
1851.708000         2.90033e+02         3.00829e+02         2.87563e+02         2.96239e+02         2.94052e+02         2.92095e+02         2.95288e+02         2.91029e+02         2.86014e+02         2.78219e+02         2.87815e+02
1851.792000         2.89128e+02         3.01310e+02         2.85011e+02         2.94961e+02         2.91011e+02         2.86653e+02         2.93762e+02         2.90788e+02         2.87245e+02         2.81626e+02         2.88544e+02
1851.875000         2.87931e+02         3.01956e+02         2.81426e+02         2.93478e+02         2.87148e+02         2.79984e+02         2.91671e+02         2.90554e+02         2.88714e+02         2.84398e+02         2.89711e+02
1851.958000         2.87491e+02         3.02349e+02         2.79810e+02         2.92074e+02         2.84512e+02         2.76075e+02         2.89838e+02         2.90589e+02         2.90470e+02         2.87509e+02         2.91155e+02
1852.042000         2.87340e+02         3.02468e+02         2.79270e+02         2.91220e+02         2.83248e+02         2.75189e+02         2.88335e+02         2.90594e+02         2.91433e+02         2.87684e+02         2.92299e+02
1852.125000         2.87478e+02         3.02466e+02         2.79175e+02         2.90781e+02         2.83326e+02         2.75991e+02         2.87956e+02         2.90827e+02         2.91631e+02         2.85738e+02         2.92993e+02
1852.208000         2.88159e+02         3.02535e+02         2.81128e+02         2.90877e+02         2.85352e+02         2.80139e+02         2.88644e+02         2.90995e+02         2.90967e+02         2.83167e+02         2.92769e+02
1852.292000         2.89317e+02         3.02595e+02         2.84167e+02         2.91774e+02         2.88841e+02         2.85948e+02         2.90668e+02         2.91393e+02         2.89792e+02         2.80497e+02         2.91940e+02
1852.375000         2.90195e+02         3.02755e+02         2.86801e+02         2.93220e+02         2.92034e+02         2.90934e+02         2.92728e+02         2.91563e+02         2.88356e+02         2.78281e+02         2.90684e+02
1852.458000         2.90913e+02         3.02851e+02         2.89253e+02         2.94847e+02         2.94860e+02         2.95472e+02         2.94473e+02         2.91582e+02         2.86966e+02         2.76430e+02         2.89400e+02
1852.542000         2.91159e+02         3.02315e+02         2.90290e+02         2.96233e+02         2.96409e+02         2.97604e+02         2.95654e+02         2.91510e+02         2.85910e+02         2.75211e+02         2.88382e+02
1852.625000         2.91086e+02         3.01069e+02         2.90048e+02         2.96964e+02         2.96381e+02         2.96618e+02         2.96231e+02         2.91505e+02         2.85791e+02         2.76501e+02         2.87938e+02
1852.708000         2.90450e+02         3.01008e+02         2.88370e+02         2.96516e+02         2.94587e+02         2.92886e+02         2.95661e+02         2.91288e+02         2.86313e+02         2.79060e+02         2.87988e+02
1852.792000         2.89398e+02         3.01803e+02         2.85263e+02         2.95350e+02         2.91551e+02         2.87378e+02         2.94186e+02         2.91066e+02         2.87245e+02         2.80902e+02         2.88711e+02
1852.875000         2.88462e+02         3.02237e+02         2.82286e+02         2.93924e+02         2.87929e+02         2.81105e+02         2.92237e+02         2.90952e+02         2.88995e+02         2.84722e+02         2.89983e+02
1852.958000         2.88027e+02         3.02613e+02         2.80993e+02         2.92314e+02         2.85268e+02         2.77490e+02         2.90178e+02         2.90864e+02         2.90786e+02         2.88214e+02         2.91381e+02
1853.042000         2.87985e+02         3.02646e+02         2.80251e+02         2.91195e+02         2.84190e+02         2.76397e+02         2.89110e+02         2.91104e+02         2.91780e+02         2.88196e+02         2.92608e+02
1853.125000         2.88026e+02         3.02542e+02         2.80248e+02         2.90719e+02         2.84160e+02         2.77153e+02         2.88585e+02         2.91162e+02         2.91892e+02         2.86630e+02         2.93108e+02
1853.208000         2.88578e+02         3.02494e+02         2.81931e+02         2.90866e+02         2.85963e+02         2.81106e+02         2.89030e+02         2.91259e+02         2.91194e+02         2.83632e+02         2.92941e+02
1853.292000         2.89584e+02         3.01814e+02         2.84699e+02         2.91669e+02         2.89013e+02         2.86168e+02         2.90809e+02         2.91553e+02         2.90154e+02         2.81669e+02         2.92115e+02
1853.375000         2.90378e+02         3.01426e+02         2.87101e+02         2.92992e+02         2.92171e+02         2.91112e+02         2.92840e+02         2.91699e+02         2.88584e+02         2.78832e+02         2.90838e+02
1853.458000         2.91101e+02         3.00696e+02         2.89815e+02         2.94564e+02         2.95030e+02         2.95861e+02         2.94506e+02         2.91619e+02         2.87171e+02         2.77351e+02         2.89440e+02
1853.542000         2.91388e+02         2.99427e+02         2.90922e+02         2.96047e+02         2.96536e+02         2.97988e+02         2.95619e+02         2.91576e+02         2.86240e+02         2.76353e+02         2.88524e+02
1853.625000         2.91016e+02         2.98523e+02         2.90027e+02         2.96909e+02         2.96331e+02         2.96724e+02         2.96083e+02         2.91414e+02         2.85700e+02         2.76219e+02         2.87891e+02
1853.708000         2.90390e+02         2.98346e+02         2.88247e+02         2.96495e+02         2.94565e+02         2.92953e+02         2.95582e+02         2.91254e+02         2.86216e+02         2.78542e+02         2.87989e+02
1853.792000         2.89396e+02         2.99011e+02         2.85398e+02         2.95408e+02         2.91615e+02         2.87664e+02         2.94109e+02         2.91008e+02         2.87178e+02         2.80726e+02         2.88669e+02
1853.875000         2.88510e+02         2.99824e+02         2.82655e+02         2.93858e+02         2.88163e+02         2.81929e+02         2.92099e+02         2.90870e+02         2.88856e+02         2.84153e+02         2.89943e+02
1853.958000         2.87825e+02         2.99969e+02         2.80347e+02         2.92197e+02         2.85263e+02         2.77242e+02         2.90327e+02         2.90841e+02         2.90387e+02         2.86747e+02         2.91229e+02
1854.042000         2.87723e+02         2.99929e+02         2.79650e+02         2.91110e+02         2.84088e+02         2.76198e+02         2.89069e+02         2.90979e+02         2.91359e+02         2.86767e+02         2.92420e+02
1854.125000         2.87841e+02         3.00041e+02         2.79845e+02         2.90896e+02         2.84110e+02         2.77192e+02         2.88478e+02         2.91066e+02         2.91572e+02         2.85314e+02         2.93018e+02
1854.208000         2.88666e+02         3.00799e+02         2.82051e+02         2.91024e+02         2.86106e+02         2.81191e+02         2.89209e+02         2.91334e+02         2.91226e+02         2.83824e+02         2.92937e+02
1854.292000         2.89647e+02         3.01291e+02         2.84864e+02         2.91489e+02         2.89141e+02         2.86438e+02         2.90848e+02         2.91575e+02         2.90153e+02         2.81621e+02         2.92124e+02
1854.375000         2.90290e+02         3.01300e+02         2.86751e+02         2.93049e+02         2.92125e+02         2.90951e+02         2.92866e+02         2.91717e+02         2.88455e+02         2.78090e+02         2.90850e+02
1854.458000         2.90944e+02         3.00720e+02         2.89198e+02         2.94588e+02         2.94833e+02         2.95382e+02         2.94487e+02         2.91648e+02         2.87055e+02         2.76449e+02         2.89506e+02
1854.542000         2.91391e+02         2.99885e+02         2.90893e+02         2.95969e+02         2.96467e+02         2.97738e+02         2.95665e+02         2.91591e+02         2.86314e+02         2.76779e+02         2.88517e+02
1854.625000         2.91159e+02         2.99091e+02         2.90299e+02         2.96884e+02         2.96491e+02         2.96822e+02         2.96282e+02         2.91506e+02         2.85827e+02         2.76849e+02         2.87901e+02
1854.708000         2.90488e+02         2.98946e+02         2.88132e+02         2.96527e+02         2.94641e+02         2.92788e+02         2.95812e+02         2.91439e+02         2.86335e+02         2.78532e+02         2.88139e+02
1854.792000         2.89403e+02         2.98944e+02         2.85129e+02         2.95546e+02         2.91589e+02         2.87343e+02         2.94269e+02         2.91126e+02         2.87217e+02         2.80564e+02         2.88755e+02
1854.875000         2.88531e+02         2.99681e+02         2.82420e+02         2.94121e+02         2.88195e+02         2.81630e+02         2.92340e+02         2.90995e+02         2.88867e+02         2.84050e+02         2.89981e+02
1854.958000         2.87969e+02         3.00226e+02         2.80470e+02         2.92468e+02         2.85394e+02         2.77307e+02         2.90499e+02         2.90992e+02         2.90544e+02         2.86991e+02         2.91364e+02

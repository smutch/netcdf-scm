.. _cli-reference:

CLI API
-------

.. click:: netcdf_scm.cli:crunch_data
   :prog: netcdf-scm-crunch
   :show-nested:

.. click:: netcdf_scm.cli:wrangle_netcdf_scm_ncs
   :prog: netcdf-scm-wrangle
   :show-nested:

.. click:: netcdf_scm.cli:stitch_netcdf_scm_ncs
   :prog: netcdf-scm-stitch
   :show-nested:
